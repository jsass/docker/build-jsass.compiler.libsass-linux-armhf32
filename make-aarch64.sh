#!/bin/bash

set -xeuo pipefail

cd /jsass/src/main

rm -fr resources/linux-aarch64
mkdir -p resources/linux-aarch64

# *** Build libsass

make -C libsass clean
cd libsass
git reset --hard # hard reset
git clean -xdf # hard clean
cd ..

# We use:
# CC=aarch64-linux-gnu-gcc
# CXX=aarch64-linux-gnu-g++
# WINDRES=aarch64-linux-gnu-gcc-ar
# BUILD="static"                      to make sure that we build a static library
CFLAGS="-Wall" \
CXXFLAGS="-Wall" \
LDFLAGS="-stdlib=libc++" \
CC=aarch64-linux-gnu-gcc \
CXX=aarch64-linux-gnu-g++ \
AR=aarch64-linux-gnu-gcc-ar \
BUILD=static \
    make -C libsass -j4

# *** Build libjsass

rm -fr c/build
mkdir -p c/build
cd c/build
cmake -DCMAKE_TOOLCHAIN_FILE=/jsass/src/main/c/Toolchain-linux-aarch64.cmake ../
make
cp libjsass.so ../../resources/linux-aarch64/libjsass.so

# *** Build libjsass_test

cd /jsass/src/test

rm -fr resources/linux-aarch64
mkdir -p resources/linux-aarch64

rm -fr c/build
mkdir -p c/build
cd c/build
cmake -DCMAKE_TOOLCHAIN_FILE=/jsass/src/main/c/Toolchain-linux-aarch64.cmake ../
make
cp libjsass_test.so ../../resources/linux-aarch64/libjsass_test.so
